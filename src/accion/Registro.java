package accion;

import com.db4o.Db4oEmbedded;
import  com.db4o.ObjectContainer;
import  com.db4o.ObjectSet;
import java.util.ArrayList;
import java.util.List;


public class Registro {
	//objeto
	private  ObjectContainer db = null;
   //abrir el archivodb
	private void abrirRegistro() {
		db = Db4oEmbedded.openFile("registroUsuario");
	}
	//cerrar la conexion
	private void cerrarRegistro() {
		db.close();
	}
	public void insertarRegistro(Persona p) {
		abrirRegistro();
		db.store(p);
		cerrarRegistro();
	}
	
	public List<Persona> seleccionarPersonas(){
		abrirRegistro();
		ObjectSet ListaPersonas = db.queryByExample(Persona.class);
		List<Persona> lp = new ArrayList<>();
		
		for(Object listaPersonas1 : ListaPersonas) {
			lp.add((Persona) listaPersonas1);
		}
		cerrarRegistro();
		return lp;
		}
	//insertas el id y nos muestra los datos de solo esa persona
	public Persona seleccionarPersona( Persona p) {
	abrirRegistro();
	ObjectSet resultado = db.queryByExample(p);
	Persona persona = (Persona) resultado.next();
	cerrarRegistro();
	return persona;
	}
	public void actualizarRegistro(int id,String Nombre, String ApellidoPat, String ApellidoMat)
	{
		abrirRegistro();
			Persona p= new Persona();
		p.setIdPersona(id);
		ObjectSet resultado = db.queryByExample(p);
		Persona preresultado = (Persona) resultado.next();
		preresultado.setNombres(Nombre);
		preresultado.setApellioPat(ApellidoPat);
		preresultado.setApellidoMat(ApellidoMat);
		db.store(preresultado);
		cerrarRegistro();
	}
	public void eliminarRegistro(int id) {
		Persona p= new Persona();
		p.setIdPersona(id);
		ObjectSet resultado = db.queryByExample(p);
		Persona preresultado = (Persona) resultado.next();
		db.delete(preresultado);
		cerrarRegistro();
	}
	
	}