package accion;

public class Persona {
	
	private int IdPersona;
	private String Nombres;
	private String ApellioPat;
	private String ApellidoMat;
	
	private int MesNacimiento;
	private int DiaNacimiento;
	private int AņoNacimiento;
	private String Sexo;
	
	

	
	private int Calle;
	private int Manzana;
	private int Lote;
	private int SuperManzana;
	private int CodigoPostal;
	private String Municipio;
	
 
	
	private String ClaveElectror;
	private String CURP;
	private int AnioRegistro;
	private int Estado;
	private int NumMunicipio;
	private int Seccion;
	private int Localida;
	private int Emision;
	private int Vigencia;
	public int getIdPersona() {
		return IdPersona;
	}
	public void setIdPersona(int idPersona) {
		IdPersona = idPersona;
	}
	public String getNombres() {
		return Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public String getApellioPat() {
		return ApellioPat;
	}
	public void setApellioPat(String apellioPat) {
		ApellioPat = apellioPat;
	}
	public String getApellidoMat() {
		return ApellidoMat;
	}
	public void setApellidoMat(String apellidoMat) {
		ApellidoMat = apellidoMat;
	}
	public int getMesNacimiento() {
		return MesNacimiento;
	}
	public void setMesNacimiento(int mesNacimiento) {
		MesNacimiento = mesNacimiento;
	}
	public int getDiaNacimiento() {
		return DiaNacimiento;
	}
	public void setDiaNacimiento(int diaNacimiento) {
		DiaNacimiento = diaNacimiento;
	}
	public int getAņoNacimiento() {
		return AņoNacimiento;
	}
	public void setAņoNacimiento(int aņoNacimiento) {
		AņoNacimiento = aņoNacimiento;
	}
	public String getSexo() {
		return Sexo;
	}
	public void setSexo(String sexo) {
		Sexo = sexo;
	}
	public int getCalle() {
		return Calle;
	}
	public void setCalle(int calle) {
		Calle = calle;
	}
	public int getManzana() {
		return Manzana;
	}
	public void setManzana(int manzana) {
		Manzana = manzana;
	}
	public int getLote() {
		return Lote;
	}
	public void setLote(int lote) {
		Lote = lote;
	}
	public int getSuperManzana() {
		return SuperManzana;
	}
	public void setSuperManzana(int superManzana) {
		SuperManzana = superManzana;
	}
	public int getCodigoPostal() {
		return CodigoPostal;
	}
	public void setCodigoPostal(int codigoPostal) {
		CodigoPostal = codigoPostal;
	}
	public String getMunicipio() {
		return Municipio;
	}
	public void setMunicipio(String municipio) {
		Municipio = municipio;
	}
	public String getClaveElectror() {
		return ClaveElectror;
	}
	public void setClaveElectror(String claveElectror) {
		ClaveElectror = claveElectror;
	}
	public String getCURP() {
		return CURP;
	}
	public void setCURP(String cURP) {
		CURP = cURP;
	}
	public int getAnioRegistro() {
		return AnioRegistro;
	}
	public void setAnioRegistro(int anioRegistro) {
		AnioRegistro = anioRegistro;
	}
	public int getEstado() {
		return Estado;
	}
	public void setEstado(int estado) {
		Estado = estado;
	}
	public int getNumMunicipio() {
		return NumMunicipio;
	}
	public void setNumMunicipio(int numMunicipio) {
		NumMunicipio = numMunicipio;
	}
	public int getSeccion() {
		return Seccion;
	}
	public void setSeccion(int seccion) {
		Seccion = seccion;
	}
	public int getLocalida() {
		return Localida;
	}
	public void setLocalida(int localida) {
		Localida = localida;
	}
	public int getEmision() {
		return Emision;
	}
	public void setEmision(int emision) {
		Emision = emision;
	}
	public int getVigencia() {
		return Vigencia;
	}
	public void setVigencia(int vigencia) {
		Vigencia = vigencia;
	}
	
	@override
	
	public String toString() {
		return "                    INSTITUTO NACIONAL ELECTORAL"+ "\n"+
	   "                      CREDENCIAL PARA VOTAR" + "\n"+ "NOMBRE \n" + Nombres +"                                      " + "FECHA DE NACIMIENTO"+  "\n " + ApellioPat  + "                                                   " + DiaNacimiento + "/" + MesNacimiento + "/" + AņoNacimiento + "\n" + 
	ApellidoMat  + "\n" + "Domicilio  \n" +"C " + Calle + " "  + "M"+ " " + Manzana + "L" +
				" " + Lote  + "\n" + "SUPMZA" +" " + SuperManzana + "  " + "Codigo Postal" + CodigoPostal + "  " + "\n" + Municipio + "\n" + "CLAVE DE ELECTOR " + " " +ClaveElectror + "\n" + "CURP " + CURP + "  " + "AŅO DE REGISTRO " + AnioRegistro
				+ "ESTADO" + Estado + " " + "MUNICIPIO" + " "+ NumMunicipio + " " + "SECCION" + Seccion +  "  " +"\n" + "LOCALIDAD" + Localida + "  " + " " + " " + "EMISION" + " " +
				Emision + " " + "  " +  "VIGENCIA" + " " + Vigencia;
	}

}
